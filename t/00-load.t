#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Geoffrey::Converter::MySQL' ) || print "Bail out!\n";
}

diag( "Testing Geoffrey::Converter::MySQL $Geoffrey::Converter::MySQL::VERSION, Perl $], $^X" );
