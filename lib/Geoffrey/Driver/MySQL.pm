package Geoffrey::Converter::MySQL;

=head1 NAME

Geoffrey::Converter::MySQL - The great new Geoffrey::Converter::MySQL!

=head1 VERSION

Version 0.000100


=cut

our $VERSION = '0.000100';

use strict;
use warnings;
use Moose;
use MooseX::HasDefaults::RO;
use MooseX::Types::PerlVersion qw( PerlVersion );

with 'Geoffrey::Role::Driver';

{

    package Geoffrey::Converter::MySQL::Tables;
    use Moose;
    use MooseX::HasDefaults::RO;
    has add   => ( isa => 'Str', default => sub { return q~CREATE TABLE {0} ( {1} )~; } );
    has drop  => ( isa => 'Str', default => sub { return q~DROP TABLE {0}~; } );
    has alter => ( isa => 'Str', default => sub { return q~ALTER TABLE {0}~; } );
    has add_column => (
        isa     => 'Str',
        lazy    => 1,
        default => sub {
            my $self = shift;
            return $self->alter . $self->str_add_column;
        }
    );
    has alter_column     => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
}
{

    package Geoffrey::Converter::MySQL::Columns;
    use Moose;
    use MooseX::HasDefaults::RO;
    has add   => ( isa => 'Str', default => sub { return q~CREATE TABLE {0} ( {1} )~; } );
    has drop  => ( isa => 'Str', default => sub { return q~DROP TABLE {0}~; } );
    has alter => ( isa => 'Str', default => sub { return q~ALTER TABLE {0}~; } );
    has add_column => (
        isa     => 'Str',
        lazy    => 1,
        default => sub {
            my $self = shift;
            return $self->alter . $self->str_add_column;
        }
    );
    has alter_column     => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
}
{

    package Geoffrey::Converter::MySQL::Constraints;
    use Moose;
    use MooseX::HasDefaults::RO;
    has add   => ( isa => 'Str', default => sub { return q~CREATE TABLE {0} ( {1} )~; } );
    has drop  => ( isa => 'Str', default => sub { return q~DROP TABLE {0}~; } );
    has alter => ( isa => 'Str', default => sub { return q~ALTER TABLE {0}~; } );
    has add_column => (
        isa     => 'Str',
        lazy    => 1,
        default => sub {
            my $self = shift;
            return $self->alter . $self->str_add_column;
        }
    );
    has alter_column     => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
}
{

    package Geoffrey::Converter::MySQL::Functions;
    use Moose;
    use MooseX::HasDefaults::RO;
    has add   => ( isa => 'Str', default => sub { return q~CREATE TABLE {0} ( {1} )~; } );
    has drop  => ( isa => 'Str', default => sub { return q~DROP TABLE {0}~; } );
    has alter => ( isa => 'Str', default => sub { return q~ALTER TABLE {0}~; } );
    has add_column => (
        isa     => 'Str',
        lazy    => 1,
        default => sub {
            my $self = shift;
            return $self->alter . $self->str_add_column;
        }
    );
    has alter_column     => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
}
{

    package Geoffrey::Converter::MySQL::Triggers;
    use Moose;
    use MooseX::HasDefaults::RO;
    has add   => ( isa => 'Str', default => sub { return q~CREATE TABLE {0} ( {1} )~; } );
    has drop  => ( isa => 'Str', default => sub { return q~DROP TABLE {0}~; } );
    has alter => ( isa => 'Str', default => sub { return q~ALTER TABLE {0}~; } );
    has add_column => (
        isa     => 'Str',
        lazy    => 1,
        default => sub {
            my $self = shift;
            return $self->alter . $self->str_add_column;
        }
    );
    has alter_column     => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
}
{

    package Geoffrey::Converter::MySQL::Indices;
    use Moose;
    use MooseX::HasDefaults::RO;
    has add   => ( isa => 'Str', default => sub { return q~CREATE TABLE {0} ( {1} )~; } );
    has drop  => ( isa => 'Str', default => sub { return q~DROP TABLE {0}~; } );
    has alter => ( isa => 'Str', default => sub { return q~ALTER TABLE {0}~; } );
    has add_column => (
        isa     => 'Str',
        lazy    => 1,
        default => sub {
            my $self = shift;
            return $self->alter . $self->str_add_column;
        }
    );
    has alter_column     => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
}
{

    package Geoffrey::Converter::MySQL::Views;
    use Moose;
    use MooseX::HasDefaults::RO;
    has add   => ( isa => 'Str', default => sub { return q~CREATE TABLE {0} ( {1} )~; } );
    has drop  => ( isa => 'Str', default => sub { return q~DROP TABLE {0}~; } );
    has alter => ( isa => 'Str', default => sub { return q~ALTER TABLE {0}~; } );
    has add_column => (
        isa     => 'Str',
        lazy    => 1,
        default => sub {
            my $self = shift;
            return $self->alter . $self->str_add_column;
        }
    );
    has alter_column     => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
    has str_add_column   => ( isa => 'Str', default => sub { return q~ ADD COLUMN {1}~; } );
    has str_alter_column => ( isa => 'Str', default => sub { return q~~; } );
}

has abstract => (
    isa     => 'SQL::Abstract',
    default => sub {
        SQL::Abstract->new;
    }
);

has listings => (
    lazy    => 1,
    default => sub {
        my $self = shift;
        return {
            table_columns      => $self->_from_abstract(),
            table_primary_keys => $self->_from_abstract(),
            table_foreign_keys => $self->_from_abstract(),
            sequences          => $self->_from_abstract(),
            indices            => $self->_from_abstract(),
            unique_keys        => $self->_from_abstract(),
            views              => $self->_from_abstract(),
            functions          => $self->_from_abstract(),
            tables             => $self->_from_abstract(),
            triggers           => $self->_from_abstract(),
            triggers_by_table  => $self->_from_abstract(),
        };
    }
);

has actions => (
    isa     => 'HashRef[ArrayRef[Str]]',
    default => sub {
        return {
            add_constraint   => [q~ADD {0}~],
            nextval_sequence => [q~DEFAULT nextval('{0}'::regclass)~],
            unique           => [q~CONSTRAINT {0} UNIQUE ({1})~],
            primary          => [q~CONSTRAINT {0} PRIMARY KEY ({1})~],
            foreign_key      => [
                q~CONSTRAINT {3} 
                    FOREIGN KEY ( {0} ) REFERENCES {1} ( {2} )
                    MATCH SIMPLE ON DELETE NO ACTION ON UPDATE NO ACTION~
            ],
        };
    }
);

has indices     => ( lazy => 1, default => sub { Geoffrey::Converter::MySQL::Indices->new } );
has views       => ( lazy => 1, default => sub { Geoffrey::Converter::MySQL::Views->new } );
has tables      => ( lazy => 1, default => sub { Geoffrey::Converter::MySQL::Tables->new } );
has functions   => ( lazy => 1, default => sub { Geoffrey::Converter::MySQL::Functions->new } );
has triggers    => ( lazy => 1, default => sub { Geoffrey::Converter::MySQL::Triggers->new } );
has constraints => ( lazy => 1, default => sub { Geoffrey::Converter::MySQL::Constraints->new } );

has defaults => (
    isa     => 'HashRef[Str]',
    default => sub {
        return {
            current     => 'now()',
            inc         => 'sequence',
            not_null    => 'NOT NULL',
            primarykey  => 'primary key',
            boolean_str => 1,
        };
    }
);

has types => (
    isa     => 'HashRef[Str]',
    default => sub {
        return {
            abstime          => 'abstime',
            aclitem          => 'aclitem',
            bigint           => 'bigint',
            bigserial        => 'bigserial',
            bit              => 'bit',
            var_bit          => 'bit varying',
            bool             => 'boolean',
            box              => 'box',
            bytea            => 'bytea',
            char             => '"char"',
            character        => 'character',
            varchar          => 'character varying',
            cid              => 'cid',
            cidr             => 'cidr',
            circle           => 'circle',
            date             => 'date',
            daterange        => 'daterange',
            decimal          => 'decimal',
            double_precision => 'double precision',
            gtsvector        => 'gtsvector',
            inet             => 'inet',
            int2vector       => 'int2vector',
            int4range        => 'int4range',
            int8range        => 'int8range',
            integer          => 'integer',
            interval         => 'interval',
            json             => 'json',
            line             => 'line',
            lseg             => 'lseg',
            macaddr          => 'macaddr',
            money            => 'money',
            name             => 'name',
            numeric          => 'numeric',
            numrange         => 'numrange',
            oid              => 'oid',
            oidvector        => 'oidvector',
            path             => 'path',
            MySQL_node_tree  => 'MySQL_node_tree',
            point            => 'point',
            polygon          => 'polygon',
            real             => 'real',
            refcursor        => 'refcursor',
            regclass         => 'regclass',
            regconfig        => 'regconfig',
            regdictionary    => 'regdictionary',
            regoper          => 'regoper',
            regoperator      => 'regoperator',
            regproc          => 'regproc',
            regprocedure     => 'regprocedure',
            regtype          => 'regtype',
            reltime          => 'reltime',
            serial           => 'serial',
            smallint         => 'smallint',
            smallserial      => 'smallserial',
            smgr             => 'smgr',
            text             => 'text',
            tid              => 'tid',
            timestamp        => 'timestamp without time zone',
            timestamp_tz     => 'timestamp with time zone',
            time             => 'time without time zone',
            time_tz          => 'time with time zone',
            tinterval        => 'tinterval',
            tsquery          => 'tsquery',
            tsrange          => 'tsrange',
            tstzrange        => 'tstzrange',
            tsvector         => 'tsvector',
            txid_snapshot    => 'txid_snapshot',
            uuid             => 'uuid',
            xid              => 'xid',
            xml              => 'xml',
        };
    }
);

has select_get_table => (
    isa     => 'Str',
    lazy    => 1,
    default => q~
        SELECT 
            t.table_name
        FROM
            information_schema.tables t
        WHERE
                table_schema='public'
            AND t.table_name = ?
        ~,
);

=head1 SUBROUTINES/METHODS

=head2 _min_version

=cut

sub _min_version { '5.5' }

=head2 _from_abstract

=cut

sub _from_abstract {
    my ( $self, $cols, $tables, $params, $type ) = @_;
    my ( $statement, @bindings ) = $self->abstract->select( $tables, $cols, $params );
    return [ $statement, \@bindings ];
}

=head2 convert_defaults

=cut

sub convert_defaults {
    my ( $self, $params ) = @_;
    $params->{default} =~ s/^'(.*)'$/$1/;
    if ( $params->{type} eq 'bit' ) {
        return qq~$params->{default}::bit~;
    }
    return $params->{default};
}

=head2 add_primary

=cut

sub add_primary {
    my ($self) = @_;
    return q~ADD ~ . $self->actions->{primary}->[0];
}

=head2 add_unique

=cut

sub add_unique {
    my ($self) = @_;
    return q~ADD ~ . $self->actions->{unique}->[0];
}

=head2 add_primary

=cut

sub add_foreignkey {
    my ($self) = @_;
    die "Foreign key is not supported!", $/ unless $self->actions->{foreign_key};
    return q~ADD ~ . $self->actions->{foreign_key}->[0];
}

=head2 parse_default

Try to convert defaults during reading from db.
To write a correct changelog file.

=cut

sub parse_default {
    my ( $self, $default_value ) = @_;
    return $1 * 1 if ( $default_value =~ m/\w'(\d+)'::"\w+"/ );
    return $default_value;
}

no Moose;

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 LICENSE AND COPYRIGHT

Copyright 2015 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, trade name, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANT ABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut
